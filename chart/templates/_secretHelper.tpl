{{- define "imagePullSecret" }}
{{- if and .Values.registry .Values.registry.password .Values.registry.username .Values.registry.host }}
{{- printf "{\"auths\": {\"%s\": {\"auth\": \"%s\"}}}" .Values.registry.host (printf "%s:%s" .Values.registry.username .Values.registry.password | b64enc) | b64enc }}
{{- else }}
{{- required "When registry is private, you need to specify .Values.registry.password .Values.registry.username .Values.registry.host, see documetation for more." "" }}
{{- end }}
{{- end }}
