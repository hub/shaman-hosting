FROM registry-gitlab.pasteur.fr/aghozlan/shaman:latest
# FROM aghozlane/shaman:latest

EXPOSE 3838

# don'y use 80 as will not run as root
RUN sed -i 's/ 80\;/ 3838;/g' /etc/shiny-server/shiny-server.conf

RUN chown shiny:shiny /var/lib/shiny-server \
 && mkdir -p /var/log/shiny-server \
 && chown shiny:shiny /var/log/shiny-server

RUN cp -r /srv/shiny-server/www/masque/ /srv/shiny-server/www/masque-default/

RUN sed -i 's|Sys.info()\["nodename"\] == "ShinyPro"|TRUE|g' /srv/shiny-server/server.R
RUN sed -i 's|socket.gethostname() == "ShinyPro"|True|g' /usr/bin/shaman_bioblend/shaman_bioblend.py
RUN sed -i 's|https://kronashiny.pasteur.fr/?parameter=|/kronarshy/?parameter=|g' /srv/shiny-server/server.R
RUN echo 'TMPDIR="/tmp-large"' >> /home/shiny/.Renviron

COPY ./*-entrypoint.sh /
RUN chmod a+x /*-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/usr/bin/shiny-server.sh"]

USER shiny