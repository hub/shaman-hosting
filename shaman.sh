#!/usr/bin/env bash
#docker run --rm \
#   -p 80:8000 \
#   -p 5438:5438 \
#   --name shaman \
#   -u 999:999 \
#   -v $(pwd)/mnt/log:/var/log \
#   -v $(pwd)/mnt/bookmarks:/var/lib/shiny-server/bookmarks \
#   -v $(pwd)/shiny-server.conf:/etc/shiny-server/shiny-server.conf \
#   aghozlane/shaman

#docker run --rm \
#   -p 80:80 \
#   -p 5438:5438 \
#   --name shaman \
#   aghozlane/shaman

mkdir -p ./mnt/log
mkdir -p ./mnt/bookmarks
mkdir -p ./mnt/masque

source token.sh
export CI_REGISTRY=registry-gitlab.pasteur.fr

#cat << EOF > token.sh
#export DEPLOY_USER="gitlab+deploy-token-000"
#export DEPLOY_TOKEN="aaaaaa"
#
#export CI_REGISTRY_PASSWORD=$DEPLOY_TOKEN
#export CI_REGISTRY_USER=$DEPLOY_USER
#EOF

echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin

# watch -n 1 ls -lah /srv/shiny-server/www/masque/*
# rm -rf /srv/shiny-server/www/masque/done/*

docker run --rm \
   -p 80:3838 \
   -p 5438:5438 \
   --name shamanp \
   -v $(pwd)/mnt/log:/var/log \
   -v $(pwd)/mnt/tmp:/tmp-large \
   -v $(pwd)/mnt/bookmarks:/var/lib/shiny-server/bookmarks \
   -v $(pwd)/mnt/masque:/srv/shiny-server/www/masque \
   shaman-patched