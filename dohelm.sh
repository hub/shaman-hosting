#!/usr/bin/env bash

touch tokens.sh
source ./tokens.sh # put `export SECRET_KEY="..."` in this file

NAMESPACE="shaman-dev"
CI_PROJECT_NAMESPACE="hub"
CI_PROJECT_NAME="shaman-hosting"
CI_REGISTRY="registry-gitlab.pasteur.fr"
CI_REGISTRY_IMAGE="${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}"
CI_COMMIT_SHA=$(git log --format="%H" -n 1)
# CI_COMMIT_SHA="63a65791c93197d280f302a67983756f91b9a1db"
#CI_COMMIT_SHA="latest"
CI_COMMIT_REF_SLUG=$(git branch --show)
#CI_COMMIT_REF_SLUG="main"
INGRESS_CLASS="internal"
PUBLIC_URL="shaman-${CI_COMMIT_REF_SLUG}.dev.pasteur.cloud"
IMAGE="${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}:${CI_COMMIT_SHA}"
IMAGE="${CI_REGISTRY_IMAGE}/main:2b08e8bd0faacfff45d5f19aa7157c64545747ce"
CHART_LOCATION="chart"
PREFIX=""

export ACTION="upgrade --install"
#export ACTION="template --debug"

helm ${ACTION} --namespace=${NAMESPACE} \
    --render-subchart-notes \
    --set ${PREFIX}ingress.className=${INGRESS_CLASS} \
    --set ${PREFIX}ingress.hostname=${PUBLIC_URL} \
    --set ${PREFIX}imageFullNameAndTag=${IMAGE} \
    --set ${PREFIX}registry.username=${DEPLOY_USER} \
    --set ${PREFIX}registry.password=${DEPLOY_TOKEN} \
    --set ${PREFIX}registry.host=${CI_REGISTRY} \
    ${CI_COMMIT_REF_SLUG} ./${CHART_LOCATION}/
